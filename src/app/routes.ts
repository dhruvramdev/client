import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { Routes } from '@angular/router' ;

export const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login' , component: LoginComponent },
  {
    path: 'signup',
    component: SignupComponent
  }
];
