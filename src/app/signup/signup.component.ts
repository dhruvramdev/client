import { Router } from '@angular/router';
import { AuthService } from './../services/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  public errorMessage ;

  constructor(private service : AuthService , private router : Router) { }

  ngOnInit() {
  }

  signup(data) {
    console.log(data);
    this.service.signup(data).subscribe(data => {
      if (data == 'OK' ) {
        this.router.navigate(['/login']);
      } else if ( !data.success ) {
        this.errorMessage = data.message ;
      }

    });
  }

}
